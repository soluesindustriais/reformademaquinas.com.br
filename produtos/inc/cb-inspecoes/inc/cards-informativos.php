<style>
    .cards-informativos{
        display: flex;
        justify-content: space-around;
        margin: 0px auto;
        max-width: 1024px;
        align-items: center;
        flex-wrap: wrap;
    }
    .cards-informativos > 
    .cards-informativos-infos{
        background-color: #fff;
        box-shadow: 0px 0px 8px #31313131;
        padding: 16px;
        border-radius: 1rem;
        width: 300px;
        margin-top: 10px;
        margin: 20px 0px;
        height: 300px;
        cursor: pointer;
        transition: 0.3s ;
    }
    .cards-informativos-infos p{
        text-align: left;
    }
    .cards-informativos-infos i{
        color: var(--color-secundary);
        font-size: 1.8rem;
    }
    .cards-informativos-infos h3{
        text-align: center;
    }
    .cards-informativos-infos:hover{
        margin-top: 0px;
        transition: 0.3s ;
    }
    .card-informativo h2, .card-informativo span p{
        text-align: center;
        font-weight: bold;
    }
    .card-informativo span p{
        font-size: 1.1rem;
        margin: 0px;
    }
    .card-informativo h2{
        margin: 0px;
        font-size: 2rem;
        color: var(--color-secundary);
    }
</style>
<section class="wrapper card-informativo">
    <span><p>O que nós fornecemos</p></span>
    <h2>Nossos principais focos</h2>
        <div class="cards-informativos">
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-layer-group"></i>
            <h3>Missão</h3>
            <p>Prover soluções de inspeção com excelência técnica, utilizando tecnologia de ponta e profissionais certificados, garantindo a segurança e a qualidade dos projetos de nossos clientes. Nosso compromisso é oferecer o melhor custo-benefício, aliando inovação e experiência.</p>
        </div>
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-eye"></i>
            <h3>Visão</h3>
            <p>Ser reconhecida como referência nacional, destacando-se pela confiabilidade, inovação tecnológica e excelência no atendimento, enquanto buscamos continuamente a melhoria e a certificação de padrões internacionais de qualidade! </p>
        </div>
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-handshake"></i>
            <h3>Valores</h3>
            <p>A CB INSPEÇÕES valoriza a excelência, investindo em tecnologia avançada e na qualificação da equipe para garantir qualidade e segurança. Oferece soluções personalizadas com o melhor custo-benefício, atuando com ética, transparência e inovação! </p>
        </div>
</div>
</section>
