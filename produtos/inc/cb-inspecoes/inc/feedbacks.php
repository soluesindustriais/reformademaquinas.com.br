<style>
     .feedbacks{
        margin: 2rem auto;
     }
    .feedback__carousel .item-slide {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        background-color: #fff;
        box-shadow: 0px 0px 8px #31313131;
        margin: 0px 32px;
        padding: 16px;
        border-radius: 1rem;
    }
     .feedbacks h2{
        text-align: center;
        margin: 12px 0px;
        font-weight: bold;
    }
   
     .feedbacks h2{
        color: var(--color-secundary);
        margin: 0px;
        font-size: 2rem;
        margin: 1.5rem 0px !important;
    }
    .feedbacks-stars .fa-star{
        margin: 0px 4px;
        font-size: 1.5rem;
        color: #FFAC1E;
    }
</style>
<?php 
function createstars($num){
    for($star = 0; $star < $num; $star++){
        echo '<i class="fa-solid fa-star"></i>';
    }
    if($num < 5){
        for($empstar = $num ; $empstar < 5; $empstar++){
            echo '<i class="fa-regular fa-star"></i>';
        }
    }
}
?>

<section class="feedbacks wrapper">

    <h2>Nossos principais focos</h2>
    <div class="feedback__carousel">
        <div class="item-slide">
            <h3>lorem ipsum dolor</h3>
            <a class="lightbox" href="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-1.webp">
                <img src="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-1.webp" alt="comprador-1">
            </a>
            <div class="feedbacks-stars"><?php createstars(5) ?></div>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi reiciendis suscipit ipsam, accusantium in magnam aperiam fuga velit rerum iure cumque ullam sapiente, dolorum similique facilis illo ex adipisci non!</p>
        </div>
        <div class="item-slide">
            <h3>lorem ipsum dolor</h3>
            <a class="lightbox" href="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-1.webp">
                <img src="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-1.webp" alt="comprador-1">
            </a>
            <div class="feedbacks-stars"><?php createstars(5) ?></div>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi reiciendis suscipit ipsam, accusantium in magnam aperiam fuga velit rerum iure cumque ullam sapiente, dolorum similique facilis illo ex adipisci non!</p>
        </div>
        <div class="item-slide">
            <h3>lorem ipsum dolor</h3>
            <a class="lightbox" href="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-1.webp">
                <img src="<?php echo $prefix_includes ?>imagens/principais-compradores/comprador-1.webp" alt="comprador-1">
            </a>
            <div class="feedbacks-stars"><?php createstars(5) ?></div>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi reiciendis suscipit ipsam, accusantium in magnam aperiam fuga velit rerum iure cumque ullam sapiente, dolorum similique facilis illo ex adipisci non!</p>
        </div>

    </div>
</section>