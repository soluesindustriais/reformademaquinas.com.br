<?php
// Infos do backup.json
$pathBackup = "$prefix_includes" . "js/menu-items-backup.json";
$pathJson = "$prefix_includes" . "js/menu-items.json";
$jsonMenu_backup = file_get_contents($pathBackup);
$menuItems_backup = json_decode($jsonMenu_backup, true);
$menuKeys_backup = array_keys($menuItems_backup);

// Se as primeira keys são iguais
if ($menuKeys == $menuKeys_backup) {
    // Keys do submenu
    foreach ($menuItems as $keys_json => $value_json) {
        if ($keys_json == "Início") {
            $nomeDoMinisite = $value_json["minisite"];
            $pastanomeDoMinisite = $value_json["pasta-minisite"];

        }
        if (isset($value_json["submenu"])) {
            $submenu_json = $value_json["submenu"];
        }
    }

    // Keys do submenu_backup
    foreach ($menuItems_backup as $keys_backup => $value_backup) {
        if (isset($value_backup["submenu"])) {
            $submenu_backup = $value_backup["submenu"];
        }
    }
    // Verifica se são diferentes

    if ($submenu_json !== $submenu_backup) {

            // Verifica quais são os itens que foram alterados
            $submenu_diference_add = array_diff_assoc($submenu_json, $submenu_backup);
            $submenu_diference_remove = array_diff_assoc($submenu_backup, $submenu_json);

            // Retorna o item para ser adicionado
            foreach($submenu_diference_add as $value => $key){
                echo "Adicionar nova página: $value ";
                $value_tratado = remove_acentos(str_replace(" ","-",strtolower(trim($value))));

                createPage_minisite($value, $value_tratado, $nomeDoMinisite, $pastanomeDoMinisite, $key);
            }

            // Retorna o item para ser removido
            foreach($submenu_diference_remove as $value => $key){
                echo "Remover a página: $value - ";
                $value = remove_acentos(str_replace(" ","-",strtolower(trim($value))));
                removePage_minisite($value);
            }
            file_put_contents($pathBackup, $jsonMenu);
    }
}

// Função que apaga os arquivos

function removePage_minisite($page){
    $page = "produtos/$page.php";
    if(file_exists($page)){
        unlink($page);
    }
}
// Funão que cria os arquivos

function createPage_minisite($page, $page_tratado, $nomeDoMinisite, $pastanomeDoMinisite,$key){
    
    $page_tratado = "produtos/$page_tratado.php";
    $content = createEstrutura($page, $nomeDoMinisite, $pastanomeDoMinisite,$key);
    file_put_contents($page_tratado, $content);
}

// Função que estrutura o contéudo da página;

function createEstrutura($page, $nomeDoMinisite, $pastanomeDoMinisite,$key){
    $page = trim($page);

    $metadescription = 'Entre em contato com $minisite para obter informações e esclarecer dúvidas com rapidez e eficiência, Solicite o seu orçamento para $h1.';
    if(isset($key["metadescription"])){
        $metadescription = $key["metadescription"];
    };

    $type = "produtos";

    if(isset($key["type"])){
        $type = $key["type"];
    };

    if($type != "produtos" && $type != "serviços"){
        $type = "produtos";
    }
    
    $conteudo = selecttype($type, $page, $nomeDoMinisite, $pastanomeDoMinisite, $metadescription);

    return $conteudo;
}


function selecttype($type, $page, $nomeDoMinisite, $pastanomeDoMinisite, $metadescription){
    if($type == "produtos"){
        $conteudo = '
    <?php
$h1 = "'.$page.'";
$title  =  $h1;
$cliente_minisite = "'.$nomeDoMinisite.'";
$minisite = "'.$pastanomeDoMinisite.'";
$desc = "'.$metadescription.'";
include "inc/$minisite/inc/head.php";
?>
</head>

<body>
    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>

    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
                <?php include "$prefix_includes" . "inc/product-images.php" ?>
                <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
            </section>
                <?php include "$prefix_includes" . "inc/product-aside.php" ?>
        </section>
        
        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>
    ';
    }
    if($type == "serviços"){
        $conteudo = '
        <?php
$h1 = "'.$page.'";
$title  =  $h1;
$cliente_minisite = "'.$nomeDoMinisite.'";
$minisite = "'.$pastanomeDoMinisite.'";
$desc = "'.$metadescription.'";
include "inc/$minisite/inc/head.php";
?>
<style>
    .extended-breadcrumb {
        background-color: var(--color-secundary);
        height: 100px;
        width: 100%;
    }

    .mpi-page {
        margin-top: -75px;
    }
</style>
</head>

<body>

    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>
    <div class="extended-breadcrumb">

    </div>
    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
            <h2>SAIBA MAIS SOBRE <?php echo $h1?></h2>
            <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
            <?php include "$prefix_includes" . "inc/imagens-mpi.php" ?>
            
            </section>
            <?php include "$prefix_includes" . "inc/aside-mpi.php" ?>
        </section>

        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>';
    }

    return $conteudo;
}
?>