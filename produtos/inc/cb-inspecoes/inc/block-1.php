<div style="background-color: var(--light);" class="container">
	<div class="wrapper">
		<section>
			<div>
				<div class="grid-col-2-3 py-5 about">
					<div class="d-flex align-items-end flex-column">
						<h1 class="about__title"><?php echo "$cliente_minisite" ?></h1>
					</div>
					<div>
						<p>A <?php echo "$cliente_minisite" ?> é líder em serviços de usinagem de alta precisão. Com tecnologia avançada e equipe especializada, oferece soluções inovadoras para diversos setores industriais, garantindo qualidade e eficiência em cada projeto.</p>
						<p><a href="<?php echo $url ?>sobre-nos" title="sobre-nos" class="btn">Mais informações</a></p>
					</div>
				</div>
			</div>
		</section>
	</div>
	<hr>
</div>
<section>
	<div>
		<div class="container">
			<div class="wrapper">
				<div class="clientes">
					<h2 class="title-underlin.about h1.about__titlee clientes__title fs-28 text-center">Nossos produtos</h2>
					<div class="produtos__carousel">
						<?php
						foreach ($menuItems as $itemName => $itemData) {
							if (isset($itemData['submenu'])) {
								foreach ($itemData['submenu'] as $indiceDestaque => $produtosDestaques) {
									echo "
                                    <div class=\"item-slide\">
						            <a href=\"$link_minisite". $itemData['submenu'][$indiceDestaque]['url'] . "\" class=\"card card--overlay\">
                						    <img class=\"card__image\" src=\"$prefix_includes" . "imagens/informacoes/" . $itemData['submenu'][$indiceDestaque]['url'] . "-1.webp\" alt=\"Página de produto: $indiceDestaque\" title=\"title\">
                                        <h3 class=\"card__title\">$indiceDestaque</h3>
                                        <span class=\"card__action\">Saiba mais</span>
                                    </a>
          						    </div>
                                    ";
								}
							}
						}
						?>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>