	<?php if (!$isMobile) : ?>
		<div class="slick-banner">
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.7) 50%, rgba(36, 36, 36, 0.7) 50%), url('<?= $url ?>imagens/banner/banner1.webp')">
				<div>
					<div class="content-banner justify-content-evenly row">
						<div>
							<h1>Radiografia Industrial com Alta Confiabilidade</h1>
							<p>Experiência comprovada para soluções de excelência.</p>
							<a class="btn" href="<?= $url ?>produtos" title="Página de produtos">Clique</a>
						</div>
						<div>
							<img src="<?= $url ?>imagens/clientes/logo-categoria.png" alt="" title="" class="slick-thumb">
						</div>
					</div>
				</div>
			</div>
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.5) 50%, rgba(36, 36, 36, 0.5) 50%), url('<?= $url ?>imagens/banner/banner2.webp')">
				<div class="content-banner">
					<h2>Segurança e Precisão em Cada Projeto</h2>
					<p>Instrumentação avançada e profissionais certificados para soluções que você pode confiar.</p>
					<a class="btn" href="<?= $url ?>sobre-nos" title="Página sobre nós">Clique</a>
				</div>
			</div>
	
		</div>
	<?php endif; ?>