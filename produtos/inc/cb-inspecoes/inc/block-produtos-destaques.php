<div style="background-color: var(--light);" class="container">
	<div class="wrapper">
		<section>
			<div>
				<div class="grid-col-2-3 py-5 about">
					<div class="d-flex align-items-end flex-column">
						<h2 class="about__title"><?php echo "$cliente_minisite" ?>
						</h2>
					</div>
					<div>
						<p>A <?php echo "$cliente_minisite" ?> é uma empresa de referência no setor de inspeção e ensaios não destrutivos, composta por uma equipe altamente qualificada e experiente. Liderada pelo Diretor Operacional Cleber Lima, certificado como Nível III em Radiografia, a empresa mantém um rigoroso controle técnico para garantir excelência e qualidade nos serviços prestados.</p>
						<p><a href="<?php echo $url ?>sobre-nos" title="Mais informações" class="btn">Mais informações</a></p>
					</div>
				</div>
			</div>
		</section>
	</div>
	<hr>
</div>
<section>
	<div>
		<div class="container">
			<div class="wrapper">
				<div class="clientes">
					<h2 class="title-underlin.about h1.about__titlee clientes__title fs-28 text-center">Nossos produtos</h2>
					<div class="produtos__carousel">
						<?php
						foreach ($menuItems as $itemName => $itemData) {
							if (isset($itemData['submenu'])) {
								foreach ($itemData['submenu'] as $indiceDestaque => $produtosDestaques) {
									echo "
                                    <div class=\"item-slide\">
						            <a href=\"$link_minisite" . $itemData['submenu'][$indiceDestaque]['url'] . "\" class=\"card card--overlay\">
                						<img class=\"card__image\" src=\"imagens/informacoes/" . $itemData['submenu'][$indiceDestaque]['url'] . "-1.webp\"  alt=\"Página de produto: $indiceDestaque\" title=\"title\">
                						<h3 class=\"card__title\">$indiceDestaque</h3>
                						<span class=\"card__action\">Saiba mais</span>
						            </a>
          						    </div>
                                    ";
								}
							}
						}
						?>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$(document).ready(function() {
		<?php if (!$isMobile): ?>
			$('.slick-banner').slick({
				fade: false,
				cssEase: 'ease',
				autoplay: true,
				infinite: true,
				speed: 500,
				dots: true,
				lazyLoad: 'ondemand',
				swipeToSlide: true,
			});
		<?php endif; ?>

		$('.produtos__carousel').slick({
			autoplaySpeed: 3000,
			autoplay: true,
			speed: 500,
			infinite: true,
			cssEase: 'ease',
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: true,
			responsive: [{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						dots: false,
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
						dots: false,
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						dots: false,
					}
				}
			]
		});

		$('.feedback__carousel').slick({
			autoplaySpeed: 3000,
			autoplay: true,
			speed: 500,
			infinite: true,
			cssEase: 'ease',
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: true,
			responsive: [{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						dots: false,
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
						dots: false,
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						dots: false,
					}
				}
			]
		});
	});
</script>