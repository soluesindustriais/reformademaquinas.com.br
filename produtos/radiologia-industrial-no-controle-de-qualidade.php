
        <?php
$h1 = "Radiologia industrial no controle de qualidade";
$title  =  $h1;
$cliente_minisite = "CB Inspeções";
$minisite = "cb-inspecoes";
$desc = "A radiologia industrial no controle de qualidade garante análises precisas, detectando falhas internas em materiais e componentes. Solicite sua cotação!";
include "inc/$minisite/inc/head.php";
?>
<style>
    .extended-breadcrumb {
        background-color: var(--color-secundary);
        height: 100px;
        width: 100%;
    }

    .mpi-page {
        margin-top: -75px;
    }
</style>
</head>

<body>

    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>
    <div class="extended-breadcrumb">

    </div>
    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
            <h2>SAIBA MAIS SOBRE <?php echo $h1?></h2>
            <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
            <?php include "$prefix_includes" . "inc/imagens-mpi.php" ?>
            
            </section>
            <?php include "$prefix_includes" . "inc/aside-mpi.php" ?>
        </section>

        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>