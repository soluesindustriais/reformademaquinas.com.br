<?$h1 = 'Serviços'; $title = 'Serviços'; $desc = 'Trocador de Calor - Saiba tudo sobre Trocador de Calor. Faça cotações com diversas tracadores de calor gratuitamente'; include('inc/head.php'); include('inc/fancy.php');?>
</head>

<body>
	<? include('inc/topo.php');?>
	<div class="wrapper">
		<main>
			<div class="content">
				<section><?=$caminho?><h1><?=$h1?></h1>
					<article class="full">
						<ul class="thumbnails-main">
							<li><a href="<?=$url?>manutencao-de-bombas-categoria"
									title="Manutenção de bombas"><img
										src="imagens/mpi/thumbs/manutencao-de-bombas-01.jpg"
										alt="Manutenção de bombas" title="Manutenção de bombas" /></a>
								<h2><a href="<?=$url?>manutencao-de-bombas-categoria"
										title="Manutenção de bombas">Manutenção de bombas</a></h2>
							</li>
							<li><a href="<?=$url?>manutencao-de-chiller-categoria"
									title="Manutenção de chiller"><img
										src="imagens/mpi/thumbs/manutencao-de-chiller-01.jpg"
										alt="Manutenção de chiller" title="Manutenção de chiller" /></a>
								<h2><a href="<?=$url?>manutencao-de-chiller-categoria"
										title="Manutenção de chiller">Manutenção de chiller</a></h2>
							</li>
							<li><a href="<?=$url?>manutencao-de-maquinas-categoria"
									title="Manutenção de máquinas"><img
										src="imagens/mpi/thumbs/manutencao-de-maquinas-01.jpg"
										alt="Manutenção de máquinas"
										title="Manutenção de máquinas" /></a>
								<h2><a href="<?=$url?>manutencao-de-maquinas-categoria"
										title="Manutenção de máquinas">Manutenção de máquinas</a></h2>
							</li>
							<li><a href="<?=$url?>manutencao-de-motores-categoria"
									title="Manutenção de motores"><img
										src="imagens/mpi/thumbs/manutencao-de-motores-01.jpg"
										alt="Manutenção de motores"
										title="Manutenção de motores" /></a>
								<h2><a href="<?=$url?>manutencao-de-motores-categoria"
										title="Manutenção de motores">Manutenção de motores</a></h2>
							</li>
							<li><a href="<?=$url?>manutencao-de-servo-motor-categoria"
									title="Manutenção de servo motor"><img
										src="imagens/mpi/thumbs/manutencao-de-servo-motor-01.jpg"
										alt="Manutenção de servo motor"
										title="Manutenção de servo motor" /></a>
								<h2><a href="<?=$url?>manutencao-de-servo-motor-categoria"
										title="Manutenção de servo motor">Manutenção de servo motor</a></h2>
							</li>
							<li><a href="<?=$url?>manutencao-industrial-categoria"
									title="Manutenção industrial"><img
										src="imagens/mpi/thumbs/manutencao-industrial-01.jpg"
										alt="Manutenção industrial"
										title="Manutenção industrial" /></a>
								<h2><a href="<?=$url?>manutencao-industrial-categoria"
										title="Manutenção industrial">Manutenção industrial</a></h2>
							</li>

						</ul>
					</article>
				</section>
			</div>
		</main>
	</div>
	<? include('inc/footer.php');?>
</body>

</html>