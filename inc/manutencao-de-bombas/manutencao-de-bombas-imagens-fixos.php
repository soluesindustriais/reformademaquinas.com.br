<div class="grid"><div class="col-6"><div class="picture-legend picture-center"><a href="<?=$url?>imagens/manutencao-de-bombas/manutencao-de-bombas-01.jpg" data-fancybox="group1" title="<?=$h1?>" target="_blank">
<picture>
  	<source type="image/webp" srcset="<?=$url?>imagens/manutencao-de-bombas/thumbs/manutencao-de-bombas-01.webp">
    <source type="image/jpeg" srcset="<?=$url?>imagens/manutencao-de-bombas/thumbs/manutencao-de-bombas-01.jpg">
    <img src="<?=$url?>imagens/manutencao-de-bombas/thumbs/manutencao-de-bombas-01.jpg" alt="<?=$h1?>" title="<?=$h1?>" />
</picture>

</a><strong>Imagem ilustrativa de <?=$h1?></strong></div> </div><div class="col-6"><div class="picture-legend picture-center"><a href="<?=$url?>imagens/manutencao-de-bombas/manutencao-de-bombas-02.jpg" data-fancybox="group1" title="<?=$h1?>" target="_blank">
<picture>
  	<source type="image/webp" srcset="<?=$url?>imagens/manutencao-de-bombas/thumbs/manutencao-de-bombas-02.webp">
    <source type="image/jpeg" srcset="<?=$url?>imagens/manutencao-de-bombas/thumbs/manutencao-de-bombas-02.jpg">
    <img src="<?=$url?>imagens/manutencao-de-bombas/thumbs/manutencao-de-bombas-02.jpg" alt="<?=$h1?>" title="<?=$h1?>" />
</picture>

</a><strong>Imagem ilustrativa de <?=$h1?></strong></div> </div></div>