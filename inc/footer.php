<footer>
    <div class="wrapper">
        <div class="contact-footer">
            <address>
                <span><?= $nomeSite . " - " . $slogan ?></span>
            </address>
        </div>
        <div class="menu-footer">
            <nav>
                <ul>
                    <li><a rel="nofollow" href="<?= $url ?>" title="Página inicial">Início</a></li>
                    <li><a rel="nofollow" href="<?= $url ?>informacoes" title="Informações">Informações</a></li>
                    <li><a rel="nofollow" href="<?= $url ?>sobre-nos" title="Sobre Nós">Sobre Nós</a></li>
                    <li><a href="<?= $url ?>mapa-site" title="Mapa do site <?= $nomeSite ?>">Mapa do site</a></li>
                </ul>
            </nav>
        </div>
        <br class="clear">
    </div>
</footer>
<div class="copyright-footer">
    <div class="wrapper-footer">
        Copyright © <?= $nomeSite ?>. (Lei 9610 de 19/02/1998)
        <div class="center-footer">
            <img src="imagens/img-home/logo-footer.png" alt="<?= $nomeSite ?>" title="<?= $nomeSite ?>">
            <p class="footer-p">é um parceiro</p>
            <img src="imagens/logo-solucs.png" alt="Soluções Industriais" title="Soluções Industriais">
        </div>
        <div class="selos">
            <a rel="nofollow" href="http://validator.w3.org/check?uri=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" target="_blank" title="HTML5 W3C"><i class="fa fa-html5"></i> <strong>W3C</strong></a>
            <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=pt-BR" target="_blank" title="CSS W3C"><i class="fa fa-css3"></i> <strong>W3C</strong></a>
        </div>
    </div>
</div>
<script src="<?= $url ?>js/jquery-1.9.0.min.js"></script>
<script src="<?= $url ?>js/vendor/modernizr-2.6.2.min.js"></script>
<script src="<?= $url ?>js/sweetalert/js/sweetalert.min.js"></script>
<script src="<?= $url ?>js/maskinput.js"></script>
<script>
    $(function() {
        $('input[name="telefone"]').mask('(99) 99999-9999');
    });
</script>
<!-- MENU  MOBILE -->
<script src="<?= $url ?>js/jquery.slicknav.js"></script>
<!-- /MENU  MOBILE -->

<!-- Shark Orcamento -->
<script>
    var guardar = document.querySelectorAll('.botao-cotar');
    for (var i = 0; i < guardar.length; i++) {
        guardar[i].removeAttribute('href');
        var adicionando = guardar[i].parentNode;
        adicionando.classList.add('nova-api');
    };
</script>
<script src="//code.jivosite.com/widget/pEmf4Ooqzw" async></script>
<!-- START API -->
<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>
<!-- END API -->


<script defer src="<?= $url ?>js/geral.js"></script>
<!-- Google Analytics -->
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-S57EMBDVNS"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-S57EMBDVNS');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-D23WW3S4NC');
</script>





<!-- BOTAO SCROLL -->
<script async src="<?= $url ?>/js/jquery.scrollUp.min.js"></script>
<script async src="<?= $url ?>/js/scroll.js"></script>
<!-- /BOTAO SCROLL -->
<script async src="<?= $url ?>/js/vendor/modernizr-2.6.2.min.js"></script>
<script async src="<?= $url ?>/js/app.js"></script>
<script src="js/click-actions.js"></script>

<!-- Script Launch start -->
<script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
<script>
    const aside = document.querySelector('aside');
    const data = `<div data-sdk-ideallaunch data-segment="Soluções Industriais - Oficial"></div>`;
    aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");
</script>
<!-- Script Launch end --><?php include 'inc/fancy.php'; ?><div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />


<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, {
                                    expires: minutesBanner
                                });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>