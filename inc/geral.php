<?
$nomeSite      = 'Reforma de Máquinas';
$slogan        = 'Dezenas de empresas para reforma de máquinas industriais';
// $url				= 'http://www.solucoesindustriais.com.br/lista/site-base/';
$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
  $url = $http . "://" . $host . "/";
} else {
  $url = $http . "://" . $host . $dir["dirname"] . "/";
}

$emailContato    = 'everton.lima@solucoesindustriais.com.br';
$rua        = 'Rua Pequetita, 179';
$bairro        = 'Vila Olimpia';
$cidade        = 'São Paulo';
$UF          = 'SP';
$cep        = 'CEP: 04552-060';
$latitude      = '-22.546052';
$longitude      = '-48.635514';
$idAnalytics    = 'UA-132717026-1';
$senhaEmailEnvia  = '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode      = explode("/", $_SERVER['PHP_SELF']);
$urlPagina       = end($explode);
$urlPagina       = str_replace('.php', '', $urlPagina);
$urlPagina       == "index" ? $urlPagina = "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
//Breadcrumbs
$caminho  = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="2">
            </li>
        </ol>
    </nav>
</div>    
</div>';
$caminho2     = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'servicos" itemprop="item" title="Serviços">
                <span itemprop="name"> Serviços</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="3">
            </li>
        </ol>
    </nav>
</div>    
</div>';
$caminhoBread  = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="2">
            </li>
        </ol>
    </nav>
</div>    
</div>';
$caminhoBread2  = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'servicos" itemprop="item" title="Serviços">
                <span itemprop="name"> Serviços</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="3">
            </li>
        </ol>
    </nav>
</div>    
</div>';
$caminhomanutencao_de_bombas = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'servicos" itemprop="item" title="Serviços">
                <span itemprop="name"> Serviços</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'manutencao-de-bombas-categoria" itemprop="item" title="Manutenção de Bombas">
                <span itemprop="name"> Manutenção de Bombas </span>
            </a>
            <meta itemprop="position" content="3">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';

$caminhomanutencao_de_chiller = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'servicos" itemprop="item" title="Serviços">
                <span itemprop="name"> Serviços</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'manutencao-de-chiller-categoria" itemprop="item" title="Manutenção de Chiller">
                <span itemprop="name"> Manutenção de Chiller </span>
            </a>
            <meta itemprop="position" content="3">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';

$caminhomanutencao_de_maquinas = '<div class="breadcrumb" id="breadcrumb">
  <div class="bread__row">
      <nav aria-label="breadcrumb">
          <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
              <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                  itemtype="https://schema.org/ListItem">
                  <a href="' . $url . '" itemprop="item" title="Home">
                      <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                  </a>
                  <meta itemprop="position" content="1">
              </li>
              <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
              itemtype="https://schema.org/ListItem">
              <a href="' . $url . 'servicos" itemprop="item" title="Serviços">
                  <span itemprop="name">Serviços</span>
              </a>
              <meta itemprop="position" content="2">
              </li>
              <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
              itemtype="https://schema.org/ListItem">
              <a href="' . $url . 'manutencao-de-maquinas-categoria" itemprop="item" title="Manutenção de Máquinas">
                  <span itemprop="name"> Manutenção de Máquinas </span>
              </a>
              <meta itemprop="position" content="3">
              </li>
              <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                  itemtype="https://schema.org/ListItem">
                  <span itemprop="name">' . $h1 . '</span>
                  <meta itemprop="position" content="4">
              </li>
          </ol>
      </nav>
  </div>    
  </div>';

$caminhomanutencao_de_motores = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'servicos" itemprop="item" title="Serviços">
                <span itemprop="name"> Serviços</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'manutencao-de-motores-categoria" itemprop="item" title="Manutenção de Motores">
                <span itemprop="name"> Manutenção de Motores </span>
            </a>
            <meta itemprop="position" content="3">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';

$caminhomanutencao_de_servo_motor = '<div class="breadcrumb" id="breadcrumb">
<div class="bread__row">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <a href="' . $url . '" itemprop="item" title="Home">
                    <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                </a>
                <meta itemprop="position" content="1">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'servicos" itemprop="item" title="Serviços">
                <span itemprop="name"> Serviços</span>
            </a>
            <meta itemprop="position" content="2">
            </li>
            <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
            itemtype="https://schema.org/ListItem">
            <a href="' . $url . 'manutencao-de-servo-motor-categoria" itemprop="item" title="Manutenção de Servo Motor">
                <span itemprop="name"> Manutenção de Servo Motor </span>
            </a>
            <meta itemprop="position" content="3">
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                itemtype="https://schema.org/ListItem">
                <span itemprop="name">' . $h1 . '</span>
                <meta itemprop="position" content="4">
            </li>
        </ol>
    </nav>
</div>    
</div>';


$caminhomanutencao_industrial = '<div class="breadcrumb" id="breadcrumb">
  <div class="bread__row">
      <nav aria-label="breadcrumb">
          <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
              <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                  itemtype="https://schema.org/ListItem">
                  <a href="' . $url . '" itemprop="item" title="Home">
                      <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                  </a>
                  <meta itemprop="position" content="1">
              </li>
              <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
              itemtype="https://schema.org/ListItem">
              <a href="' . $url . 'servicos" itemprop="item" title="Serviços">
                  <span itemprop="name"> Serviços</span>
              </a>
              <meta itemprop="position" content="2">
              </li>
              <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
              itemtype="https://schema.org/ListItem">
              <a href="' . $url . 'manutencao-industrial-categoria" itemprop="item" title="Manutenção Industrial">
                  <span itemprop="name"> Manutenção Industrial </span>
              </a>
              <meta itemprop="position" content="3">
              </li>
              <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                  itemtype="https://schema.org/ListItem">
                  <span itemprop="name">' . $h1 . '</span>
                  <meta itemprop="position" content="4">
              </li>
          </ol>
      </nav>
  </div>    
  </div>';

  $caminhoinformacoes = '<div class="breadcrumb" id="breadcrumb">
  <div class="bread__row">
      <nav aria-label="breadcrumb">
          <ol class="breadcrumb item-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
              <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
                  itemtype="https://schema.org/ListItem">
                  <a href="' . $url . '" itemprop="item" title="Home">
                      <span itemprop="name"><i class="fa-solid fa-house"></i>Início</span>
                  </a>
                  <meta itemprop="position" content="1">
              </li>
              <li class="breadcrumb-item"  itemprop="itemListElement" itemscope
              itemtype="https://schema.org/ListItem">
              <a href="' . $url . 'informacoes" itemprop="item" title="Informações">
                  <span itemprop="name"> Informações</span>
              </a>
              <meta itemprop="position" content="2">
              </li>
              <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                  itemtype="https://schema.org/ListItem">
                  <span itemprop="name">' . $h1 . '</span>
                  <meta itemprop="position" content="3">
              </li>
          </ol>
      </nav>
  </div>    
  </div>';