<?php
$h1 = "Carrinho";
$title = $h1;
$desc = "Carrinho - Lista de itens do orçamento de produtos comercializados. Entre em contato para solicitar um orçamento. Clique aqui e conheça nossa linha de produtos.";
$key = "carrinho, produtos, orçamento";
$pagInterna = "Carrinho";
$urlPagInterna = "carrinho";
include('inc/head.php');
include('inc/form-scripts.php');
?>
</head>
<body>
  <?php include('inc/topo.php'); ?>
  <main>
    <div id="carrinho">
      <section>
        <?= $caminho ?>
        <div class="container">
          <div class="wrapper">
            <?php
            $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            define('POST', $post);
            function RecoverForm($contentType){
              return isset(POST[$contentType]) ? POST[$contentType] : '';
            }
            if (isset($post) && isset($post['enviar_orc'])):
              $post['user_empresa'] = EMPRESA_CLIENTE;
          //Inclue o verificador de Spammers do formulário
            include('inc/searchSpammer.inc.php');
          //Armazena o reCapcha
            $recapt = $post['g-recaptcha-response'];
          //Remove o submit e o reCpatcha para validação de campos vazios
            unset($post['enviar_orc'], $post['g-recaptcha-response']);
          //Arquivos válidos que podem ser enviados
            $MimeTypes = array(
              'application/pdf',
              'application/msword',
              'application/vnd.ms-excel',
              'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
              'application/x-excel',
              'application/x-msexcel',
              'image/png',
              'image/pjpeg',
              'image/jpeg',
              'image/jpg',
              'image/pjpeg',
              'image/gif'
            );
            $post['orc_mensagem'] = $post['orc_mensagem'] == '' ? 'Não informado' : $post['orc_mensagem'];
          //Verifica se os campos obrigatórios foram todos preenchidos
            if (in_array('', $post)):
              echo '<script>'
              . '$(function () {';
              echo 'swal("Aviso!", "Campos com * são obrigatórios.", "info");';
              echo '});'
              . '</script>';
          //Verifica se existem spammers nos campos do formulário
            elseif (SearchSpammer($post)):
          //Inclui o emailFake, que fará a notificação aos adms do site
              include('inc/emailFake.inc.php');
          //Verifica se existe anexo para envio e se o anexo está na lista do MimeTypes
            elseif (isset($_FILES['anexo']) && !empty($_FILES['anexo']['tmp_name']) && !in_array($_FILES['anexo']['type'], $MimeTypes)):
              echo '<script>'
            . '$(function () {';
            echo 'swal("Aviso!", "Escolha um arquivo válido para enviar como anexo da mensagem", "info");';
            echo '});'
            . '</script>';
          else:
            $Contact = new Orcamento($post);
            $error = $Contact->getError();
            if (!$Contact->getResult()):
              WSErro($error[0], $error[1]);
            else:
          //Caso as condições sejam atendidas, o reCaptcha volta para o post e o anexo é adicionado ao post
              $post['g-recaptcha-response'] = $recapt;
              include("inc/carrinho-envia-inc.php");
            endif;
          endif;
        endif; ?>
        <?php if (count($_SESSION['CARRINHO']) == 0): ?>
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Opss!!</h3>
            </div>
            <div class="panel-body">Desculpe, mas não existem produtos no carrinho de orçamento.</div>
          </div>
          <button class="btn btn_orc" onclick="location = '<?=RAIZ?>/produtos'">Ir para produtos</button>
        <? else: ?>
          <h2>CONFIRA ABAIXO OS ITENS DE SEU ORÇAMENTO</h2>
          <div class="carrinho j_carrinho">
            <div class="row">
              <div class="col-12 carrinho__head"> PRODUTO (S) </div>
              <?php foreach ($_SESSION['CARRINHO'] as $id => $value): ?>
                <div class="row col-12 carrinho__body" id="<?=$id?>">
                  <div class="col-7 col-sm-7 carrinho__row">
                    <!-- Exibe se tiver prod_código -->
                    <?php if ($value['prod_codigo'] && $value['prod_codigo'] != ' '): ?>
                      <span>Ref..: <?=$value['prod_codigo']?></span>
                    <?php endif ?>
                    <!-- Exibe prod_title -->
                    <span><?= Check::Words($value['prod_title'], 8); ?></span>
                    <!-- Exibe se tiver prod_atributos -->
                    <?php foreach ($value['modelos'] as $itens => $qtd): ?>
                      <?php if ($itens): ?>
                        <span id="<?= $id . $itens; ?>">Aplicação: <?= $itens; ?></span>
                      <?php endif ?>
                    <? endforeach; ?>
                  </div>
                  <div class="col-5 col-sm-5 carrinho__row">
                    <div class="row justify-content-between">
                      <div class="p-2 col-6 col-sm-6 col-xs-8">
                        <!-- Exibe número de itens adicionados -->
                        <?php foreach ($value['modelos'] as $itens => $qtd): ?>
                          <input class="qtdCart j_qtdCart" type="number" name="prod_qtd" value="<?=$qtd?>" data-qtd="<?=$id?>" min="1" step="1"  title="<?=$itens?>" />
                        <? endforeach; ?>
                      </div>
                      <div class="p-2 col-4 col-sm-4 col-xs-4">
                        <!-- Botão excluir do orçamento -->
                        <button class="removeCart j_removeCart" tabindex="<?=$id?>">
                          <i class="fas fa-trash fa-1x"></i>
                        </button>
                        <input type="hidden" class="j_base" value="<?=BASE?>"/>
                      </div>
                    </div>
                  </div>
                </div>
              <? endforeach; ?>
            </div>
          </div>
          <div class="d-flex gap-20">
            <button class="btn btn_orc" onclick="location = '<?=RAIZ?>/produtos'"><i class="fas fa-shopping-cart"></i> Selecionar mais itens</button>
            <button class="btn btn_orc j_solicitar"><i class="fas fa-envelope"></i> Solicitar orçamento</button>
          </div>
          <br class="clear">
          <form method="post" class="formulario j_formulario">
            <span class="p-2 dark d-block large">PREENCHA O FORMULÁRIO ABAIXO:</span>
            <div class="row">
              <div class="p-2 col-6">
                <label>Nome: *</label>
                <input type="text" name="orc_nome" placeholder="Seu nome completo" value="<?=RecoverForm('orc_nome');?>" required />
              </div>
              <div class="p-2 col-6">
                <label>E-mail: *</label>
                <input type="email" name="orc_email" placeholder="Ex: exemplo@exemplo.com" value="<?=RecoverForm('orc_email');?>" required />
              </div>
              <!-- <div class="p-2 col-4">
                <label>Telefone: *</label>
                <input type="tel" name="orc_telefone" placeholder="Ex: (00) 0000-0000" value="<?=RecoverForm('orc_telefone');?>" required />
              </div> -->
              <div class="p-2 col-4">
                <label>Celular: *</label>
                <input type="tel" name="orc_celular" placeholder="Ex: (00) 00000-0000" value="<?=RecoverForm('orc_celular');?>" required />
              </div>
              <div class="p-2 col-6">
                <label>Cidade: *</label>
                <input id="cidade" type="text" name="orc_cidade" placeholder="Ex: São Paulo" value="<?=RecoverForm('orc_cidade');?>" required />
              </div>
              <div class="p-2 col-6">
                <label>Estado: *</label>
                <select name="orc_uf" required>
                  <option value="">-- Selecione --</option>
                  <option value="AC">AC</option>
                  <option value="AL">AL</option>
                  <option value="AP">AP</option>
                  <option value="AM">AM</option>
                  <option value="BA">BA</option>
                  <option value="CE">CE</option>
                  <option value="DF">DF</option>
                  <option value="ES">ES</option>
                  <option value="GO">GO</option>
                  <option value="MA">MA</option>
                  <option value="MT">MT</option>
                  <option value="MS">MS</option>
                  <option value="MG">MG</option>
                  <option value="PA">PA</option>
                  <option value="PB">PB</option>
                  <option value="PR">PR</option>
                  <option value="PE">PE</option>
                  <option value="PI">PI</option>
                  <option value="RJ">RJ</option>
                  <option value="RN">RN</option>
                  <option value="RS">RS</option>
                  <option value="RO">RO</option>
                  <option value="RR">RR</option>
                  <option value="SC">SC</option>
                  <option value="SP">SP</option>
                  <option value="SE">SE</option>
                  <option value="TO">TO</option>
                </select>
              </div>
              <div class="p-2 col-6">
                <label>Como nos conheceu? *</label>
                <select name="orc_question" required>
                  <option value="">-- Selecione --</option>
                  <option value="Busca do Google">Busca do Google</option>
                  <option value="Outros Buscadores">Outros Buscadores</option>
                  <option value="Links patrocinados">Links patrocinados</option>
                  <option value="Outros Anúncios">Outros Anúncios</option>
                  <option value="Facebook">Facebook</option>
                  <option value="Twitter">Twitter</option>
                  <option value="Google+">Google+</option>
                  <option value="Indicação">Indicação</option>
                  <option value="Outros">Outros</option>
                </select>
              </div>
              <!-- <div class="p-2 col-6">
                <label>Segmento: *</label>
                <select name="orc_segmento" required>
                  <option value="">-- Selecione --</option>
                  <option value="Residencial">Residencial</option>
                  <option value="Comercial">Comercial</option>
                  <option value="Empresarial">Empresarial</option>
                </select>
              </div> -->
              <div class="p-2 col-12">
                <label>Mensagem:</label>
                <textarea name="orc_mensagem" placeholder="Se preferir especificar alguma necessidade, digite aqui." rows="4"><?=RecoverForm('orc_mensagem');?></textarea>
              </div>
              <div class="p-2 col-12">
                <span class="formulario__obrigatory">Os campos com * são obrigatórios</span>
                <div class="g-recaptcha" data-size="<?= (!$isMobile) ? 'normal' : 'compact' ?>" data-sitekey="<?=$siteKey?>"></div>
                <input class="ir" name="enviar_orc" type="submit" value="Enviar"/>
              </div>
            </div>
          </form>
        <?php endif; ?>
      </div> <!-- wrapper -->
      <div class="clear"></div>
    </div> <!-- container -->
  </section>
</div> <!-- carrinho -->
</main>
<?php include('inc/footer.php'); ?>
<script>
  $(document).ready(function(){
    $('input[name="orc_telefone"]').mask('(99) 9999-9999');
    $('input[name="orc_celular"]').mask('(99) 99999-9999');
    $('input[name="orc_cep"]').mask('00000-000');
  });
</script>
</body>
</html>