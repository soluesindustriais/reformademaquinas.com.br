<form class="form-horizontal" method="post">
  <fieldset>
    <div class="control-group">
      <div class="controls col-lg-8 col-md-7 col-sm-7 col-xs-12">
        <div class="input-prepend input-group">
          <span class="add-on input-group-addon"><i class="fa fa-search"></i></span>
          <input type="search" name="busca" class="form-control" required placeholder="Pesquisar por..."/>
        </div>                    
      </div>
      <div class="col-lg-4 col-md-5 col-sm-5 col-xs-12">                    
        <div class="btn-group pull-right">
          <button type="submit" name="pesquisar" value="Pesquisar" class="btn btn-success"><i class="fa fa-search"></i> Pesquisar</button>
          <button type="button" onclick="location = 'painel.php?exe=<?= $linkto[0]; ?>/index'" class="btn btn-primary"><i class="fa fa-refresh"></i> Mostrar todos</button>
        </div>
      </div>
    </div>
  </fieldset>

  <div class="clearfix"></div>
  <hr>

  <div class="col-lg-4 col-md-12 col-xs-12 pull-right"> 
    <div class="form-group ">
      <label class="control-label col-md-4" for="PerPage">Exibir</label>
      <div class="col-md-4">
        <select name="PerPage" class="form-control col-md-4 input-sm j_change" data-url="<?= BASE . "/painel.php?exe=" . $linkto[0] . "/index"; ?>">
          <option value="25" <?php
          if (isset($PerPage) && $PerPage == 25): echo 'selected="selected"';
          endif;
          ?>>25</option>
          <option value="50" <?php
          if (isset($PerPage) && $PerPage == 50): echo 'selected="selected"';
          endif;
          ?>>50</option>
          <option value="75" <?php
          if (isset($PerPage) && $PerPage == 75): echo 'selected="selected"';
          endif;
          ?>>75</option>
          <option value="125" <?php
          if (isset($PerPage) && $PerPage == 125): echo 'selected="selected"';
          endif;
          ?>>125</option>
          <option value="250" <?php
          if (isset($PerPage) && $PerPage == 250): echo 'selected="selected"';
          endif;
          ?>>250</option>
          <option value="300" <?php
          if (isset($PerPage) && $PerPage == 300): echo 'selected="selected"';
          endif;
          ?>>300</option>
        </select>                      
      </div>
      <label class="control-label col-md-4" for="PerPage">Por página</label>
    </div>
  </div>
</form>

