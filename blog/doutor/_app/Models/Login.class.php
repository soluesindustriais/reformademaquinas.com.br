<?php

/**
 * Login.class [ MODEL ]
 * Responável por autenticar, validar, e checar usuário do sistema de login!
 * 
 * @copyright (c) 2014, Robson V. Leite UPINSIDE TECNOLOGIA
 */
class Login {

    private $Level;
    private $Email;
    private $Senha;
    private $Error;
    private $Result;
    private $Userid;

    /**
     * <b>Informar Level:</b> Informe o nível de acesso mínimo para a área a ser protegida.
     * @param INT $Level = Nível mínimo para acesso
     */
    function __construct($Level) {
        $this->Level = (int) $Level;
    }

    /**
     * <b>Efetuar Login:</b> Envelope um array atribuitivo com índices STRING user [email], STRING pass.
     * Ao passar este array na ExeLogin() os dados são verificados e o login é feito!
     * @param ARRAY $UserData = user [email], pass
     */
    public function ExeLogin(array $UserData) {
        $this->Email = (string) strip_tags(trim($UserData['user']));
        $this->Senha = (string) strip_tags(trim($UserData['pass']));
        $this->setLogin();
    }

    /**
     * <b>Função que atualiza a data do ultimo acesso</b>
     * Esta função é executada quando é efetuado o login e se efetuado o logout
     * Guarda a data no formato TimeStamp atual do servidor
     */
    public function LastAcess() {
        $this->Userid = $_SESSION['userlogin']['user_id'];
        $Dados = array('user_lastacccess' => date("Y-m-d H:i:s"));
        $Update = new Update;
        $Update->ExeUpdate(TB_USERS, $Dados, "WHERE user_id = :id", "id={$this->Userid}");
    }

    /**
     * <b>Verificar Login:</b> Executando um getResult é possível verificar se foi ou não efetuado
     * o acesso com os dados.
     * @return BOOL $Var = true para login e false para erro
     */
    public function getResult() {
        return $this->Result;
    }

    /**
     * <b>Obter Erro:</b> Retorna um array associativo com uma mensagem e um tipo de erro WS_.
     * @return ARRAY $Error = Array associatico com o erro
     */
    public function getError() {
        return $this->Error;
    }

    /**
     * <b>Checar Login:</b> Execute esse método para verificar a sessão USERLOGIN e revalidar o acesso
     * para proteger telas restritas.
     * @return BOLEAM $login = Retorna true ou mata a sessão e retorna false!
     */
    public function CheckLogin() {
        if (empty($_SESSION['userlogin']) || $_SESSION['userlogin']['user_level'] < $this->Level):
            unset($_SESSION['userlogin']);
            return false;
        else:
            return true;
        endif;
    }

    /*
     * ***************************************
     * **********  PRIVATE METHODS  **********
     * ***************************************
     */

    //Valida os dados e armazena os erros caso existam. Executa o login!
    private function setLogin() {
        if (!$this->Email || !$this->Senha || !Check::Email($this->Email)):
            $this->Error = array('Informe seu e-mail e senha.', WS_INFOR, null, "MPI Technology");
            $this->Result = false;
        elseif (!$this->getUser()):
            $this->Error = array('Dados de acesso incorretos.', WS_ALERT, null, "MPI Technology");
            $this->Result = false;
        elseif (!$this->getEmpStatus()):
            $this->Error = array('Acesso desativado.', WS_ERROR, null, "MPI Technology");
            $this->Result = false;
        elseif ($this->Result['user_level'] < $this->Level):
            $this->Error = array("Desculpe {$this->Result['user_name']}, você não tem permissão para acessar esta área.", WS_ERROR, null, "MPI Technology");
            $this->Result = false;
        else:
            $this->Execute();
        endif;
    }

    //Vetifica usuário e senha no banco de dados!
    private function getUser() {
        $this->Senha = md5($this->Senha);

        $read = new Read;
        $read->ExeRead(TB_USERS, "WHERE user_email = :e AND user_password = :p", "e={$this->Email}&p={$this->Senha}");

        if ($read->getResult()):
            $this->Result = $read->getResult();
            $this->Result = $this->Result[0];
            return true;
        else:
            return false;
        endif;
    }

    //Verifica se a empresa foi desativada no sistema
    private function getEmpStatus() {
        $this->Senha = md5($this->Senha);

        $user = $this->Result['user_empresa'];
        $read = new Read;
        $read->ExeRead(TB_EMP, "WHERE empresa_id = :id", "id={$user}");
        if (!$read->getResult()):
            return false;
        else:
            $result = $read->getResult();
            if ($result[0]['empresa_status'] == 2):
                return false;
            else:
                return true;
            endif;
        endif;
    }

    //Executa o login armazenando a sessão!
    private function Execute() {
        if (!session_id()):
            session_start();
        endif;

        $_SESSION['userlogin'] = $this->Result;
        Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Login", "O usuário efetuou o login no sistema", date("Y-m-d H:i:s"));
        $this->Error = array("Olá {$this->Result['user_name']}!", WS_ACCEPT, null, "CMS - Doutores da Web");
        $this->Result = true;
    }

}
