<?
$h1         = 'Blog';
$title      = 'Blog';
$desc       = 'Blog - Reforma de Máquinas - Realize orçamentos de Reforma de Máquinas, ache as melhores fábricas, receba diversas cotações imediatamente com aproximadamente 100 distribuidores de todo o Brasil';
$key        = 'uuuuuuuuuu, jjjjjjjjjjjj, lllllllllll';
$var        = 'Blog';
include('inc/head.php');
?>
<!--STARTSCRIPTSHEADER-->
<style>
	<?php
	if(!$isMobile):
		include('slick/slick.css');
		include('slick/slick-banner.css');
	endif;

	include('inc/blog-home-styles.php'); 
	?>
</style>
<!--ENDSCRIPTSHEADER-->
</head>
<body>
	<? include('inc/topo-blog.php'); ?>
	<main>
		<?php if(!$isMobile): ?>
			<!--STARTBANNER-->
			<? include('inc/banner-inc.php'); ?>
			<!--ENDBANNER-->
		<?php endif; ?>
		<div class="content">
			<section>
				<!--STARTCOMPONENTS-->
				<div class="bg-primary-color">
					<div class="container">
						<div class="wrapper">
							<div class="blog-home">
								<h1 class="blog-home__title"><?=$h1?><span class="d-block dark text-center mt-5"><?=$nomeSite?></span></h1>
							</div>
						</div>
					</div>
				</div>
				
				<? include('inc/blog-home-themes.php'); ?>
				
				<!--ENDCOMPONENTS-->
			</section>
		</div> <!-- end content -->
	</main>
	<? include('inc/footer-blog.php'); ?>
	<!--STARTSCRIPTSFOOTER-->
	<script>
		<? include('slick/slick.min.js'); ?>
	</script>
	<script>
		$(document).ready(function(){
			<?php if(!$isMobile): ?>
				$('.slick-banner').slick({
					fade: true,
					cssEase: 'ease',
					autoplay: true,
					infinite: true,
					speed: 1000,
					dots: true,
					lazyLoad: 'ondemand',										
					swipeToSlide: true,		
				});
			<?php endif; ?>
		});
	</script>
	<!--ENDSCRIPTSFOOTER-->
</body>
</html>
