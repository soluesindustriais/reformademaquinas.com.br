<?
$h1         = 'Manutenção de máquinas';
$title      = 'Início';
$desc       = 'Realize orçamentos de $h1, ache as melhores fábricas, receba diversas cotações imediatamente com aproximadamente 100 distribuidores de todo o Brasil';
$var        = 'Home';
include('inc/head.php');
include('inc/fancy.php');
?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
	<div class="title-main"> <h1>Reforma de máquinas</h1> </div>
	<ul class="cd-hero-slider autoplay">
		<li class="selected">
			<div class="cd-full-width">
				<h2>Manutenção de Motores</h2>
				<p>A manutenção de motores elétricos é um serviço feito por profissionais capacitados e experientes, sendo essencial a contratação de uma empresa especializada na área para a execução. O serviço de manutenção desses motores é prático e rápido, além de ter um custo acessível.</p>
				<a href="<?=$url?>manutencao-de-motores-categoria" class="cd-btn">Saiba mais</a>
			</div>
		</li>
		<li>
			<div class="cd-full-width">
				<h2>Manutenção Industrial</h2>
				<p>Os painéis elétricos são responsáveis por verificar, simplificar e administrar a energia que vem da fonte de alimentação de uma instalação industrial. Esse equipamento necessita ser montado de acordo com estudos e análises de toda a aparelhagem.</p>
				<a href="<?=$url?>manutencao-industrial" class="cd-btn">Saiba mais</a>
			</div>
		</li>
		<li>
			<div class="cd-full-width">
				<h2>Reforma de Bombas</h2>
				<p>A bomba submersa é um equipamento que funciona dentro da água. A principal função das bombas submersas é pressionar líquidos — principalmente a água — durante o processo de bombeamento.</p>
				<a href="<?=$url?>reforma-de-bombas-de-agua-sp" class="cd-btn">Saiba mais</a>
			</div>
		</li>
	</ul>
	<div class="cd-slider-nav">
		<nav>
			<span class="cd-marker item-1"></span>
			<ul>
				<li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
				<li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
				<li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
			</ul>
		</nav>
	</div>
</section>
<main>
	<section class="wrapper-main">
		<div class="main-center">
			<div class=" quadro-2 ">
				<h2>Reforma de máquinas</h2>
				<div class="div-img">
					<p data-anime="left-0">A reforma de máquinas é um serviço muito solicitado por indústrias, empresas de pequeno a grande porte, e deve ser realizado por uma equipe técnica altamente qualificada composta por engenheiros mecânicos e eletrônicos que estão sempre em constante atualização, buscando novas tecnologias, e seguindo as inovações do mercado.</p>
				</div>
				<div class="gerador-svg" data-anime="in">
					<img src="imagens/img-home/processo-reforma-maquina.jpg" alt="Reforma de máquina" title="Reforma de máquina">
				</div>
			</div>
			<div class=" incomplete-box">
				<ul data-anime="in">
					<li>
						<p>A missão desses profissionais é manter as principais funcionalidades da máquinas, eliminando desgastes, peças danificadas, adaptação a itens e normas de segurança vigentes, e trocando os componentes eletroeletrônicos por outros de última geração.</p>
						
						<li><i class="fas fa-angle-right"></i>Aumento de Produtividade</li>
						<li><i class="fas fa-angle-right"></i>Integração em redes</li>
						<li><i class="fas fa-angle-right"></i>Redução de riscos</li>
						<li><i class="fas fa-angle-right"></i>Redução de custos e aumento de produtividade</li>
					</ul>
					<a href="<?=$url?>reforma-de-maquinas-cnc" class="btn-4">Saiba mais</a>
				</div>
			</div>
			<div id="content-icons">
				<div class="co-icon">
					<div class="quadro-icons" data-anime="in">
						<i class="fa fa-usd" aria-hidden="true"></i>
						<div>
							<p>Compare preços</p>
						</div>
					</div>
				</div>
				<div class="co-icon">
					<div class="quadro-icons" data-anime="in">
						<i class="fa fa-clock-o" aria-hidden="true"></i>
						<div>
							<p>Economize tempo</p>
						</div>
					</div>
				</div>
				<div class="co-icon">
					<div class="quadro-icons" data-anime="in">
						<i class="fa fa-handshake-o" aria-hidden="true"></i>
						<div>
							<p>Faça o melhor negócio</p>
						</div>
					</div>
				</div>
			</div>
			<section class="wrapper-img">
				<div class="txtcenter">
					<h2>Produtos <b>Relacionados</b></h2>
				</div>
				<div class="content-icons">
					<div class="produtos-relacionados-1">
						<figure>
							<a href="<?=$url?>conserto-de-chiller">
								<div class="fig-img">
									<h2>Reforma de chiller</h2>
									<div class="btn-5" data-anime="up"> Saiba Mais </div>
								</div>
							</a>
						</figure>
					</div>
					<div class="produtos-relacionados-2">
						<figure class="figure2">
							<a href="<?=$url?>manutencao-de-servo-motor-categoria">
								<div class="fig-img2">
									<h2 class="concerto-maquina">Conserto Servo Motor</h2>
									<div class="btn-5" data-anime="up"> Saiba Mais </div>
								</div>
							</a>
						</figure>
					</div>
					<div class="produtos-relacionados-3">
						<figure>
							<a href="<?=$url?>manutencao-de-maquinas-cnc">
								<div class="fig-img">
									<h2>Reforma máquinas CNC</h2>
									<div class="btn-5" data-anime="up"> Saiba Mais </div>
								</div>
							</a>
						</figure>
					</div>
				</div>
			</section>
			<section class="wrapper-destaque">
				<div class="destaque txtcenter">
					<h2>Galeria de <b>Produtos</b></h2>
					<div class="center-block txtcenter">
						<ul class="gallery">
							<li>
								<a href="<?=$url?>imagens/img-home/thumbs/retrofit-de-maquinas.jpg" class="lightbox" data-fancybox="group1" title="Retrofit de máquinas">
									<img src="<?=$url?>imagens/img-home/thumbs/retrofit-de-maquinas.jpg" title="Retrofit de máquinas" alt="Retrofit de máquinas">
								</a>
							</li>
							<li>
							<a href="<?=$url?>imagens/img-home/thumbs/bomba-de-piscina.jpg" class="lightbox" data-fancybox="group1"  title="Bomba de piscina">
								<img src="<?=$url?>imagens/img-home/thumbs/bomba-de-piscina.jpg" alt="Bomba de piscina" title="Bomba de piscina">
							</a>
							</li>
					<li><a href="<?=$url?>imagens/img-home/thumbs/bomba-submersa.jpg" class="lightbox" data-fancybox="group1" title="Bomba submersa">
						<img src="<?=$url?>imagens/img-home/thumbs/bomba-submersa.jpg" alt="Bomba submersa" title="Bomba submersa">
					</a>
				</li>
				<li><a href="<?=$url?>imagens/img-home/thumbs/painel-eletrico.jpg" class="lightbox" data-fancybox="group1" title="Painel elétrico">
					<img src="<?=$url?>imagens/img-home/thumbs/painel-eletrico.jpg" alt="Painel elétrico" title="Painel elétrico">
				</a>
			</li>
			<li><a href="<?=$url?>imagens/img-home/thumbs/motor-eletrico.jpg" class="lightbox" data-fancybox="group1" title="Motor elétrico">
				<img src="<?=$url?>imagens/img-home/thumbs/motor-eletrico.jpg" alt="Motor elétrico"  title="Motor elétrico">
			</a>
		</li>
		<li><a href="<?=$url?>imagens/img-home/thumbs/usinagem-torno.jpg" class="lightbox" data-fancybox="group1" title="Usinagem torno">
			<img src="<?=$url?>imagens/img-home/thumbs/usinagem-torno.jpg" alt="Usinagem torno" title="Usinagem torno">
		</a>
	</li>
	<li><a href="<?=$url?>imagens/img-home/thumbs/maquina-industrial.jpg" class="lightbox" data-fancybox="group1" title="Máquina elétrica industrial">
		<img src="<?=$url?>imagens/img-home/thumbs/maquina-industrial.jpg" alt="Máquina industrial" title="Máquina elétrica industrial">
	</a>
</li>
<li><a href="<?=$url?>imagens/img-home/thumbs/circuito-eletronico.jpg" class="lightbox" data-fancybox="group1" title="Circuito eletrônico">
	<img src="<?=$url?>imagens/img-home/thumbs/circuito-eletronico.jpg" alt="Circuito eletrônico" title="Circuito eletrônico">
</a>
</li>
<li><a href="<?=$url?>imagens/img-home/thumbs/maquina-laser.jpg" class="lightbox" data-fancybox="group1" title="Máquina laser">
<img src="<?=$url?>imagens/img-home/thumbs/maquina-laser.jpg" alt="máquina laser" title="Máquina laser">
</a>
</li>
<li><a href="<?=$url?>imagens/img-home/thumbs/maquina-solda.jpg" class="lightbox" data-fancybox="group1" title="Máquina de solda">
<img src="<?=$url?>imagens/img-home/thumbs/maquina-solda.jpg" alt="Máquina de solda" title="Máquina de solda">
</a>
</li>
</ul>
</div>
</div>
</section>
</section>
</main>
<? include('inc/footer.php'); ?>
<link rel="stylesheet" href="<?=$url?>nivo/nivo-slider.css" media="screen">
<script  src="<?=$url?>nivo/jquery.nivo.slider.js"></script>
<script >
$(window).load(function() {
$('#slider').nivoSlider();
});
</script>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
</body>
</html>